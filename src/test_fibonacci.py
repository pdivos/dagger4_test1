from dagger4 import DLambda, call, Fiddle, DepCollector

@DLambda
def fib(n):
    # classic Fibonacci, calls dependencies recursively
    # if the value of a dependency is available, call() will return immediately
    # otherwise the execution will be terminated, missing dep will be evaluated
    # and execution is re-attempted when missing dep is in.
    # therefore this function is started potentially 3 times
    if n <= 1: return n
    DepCollector().require('fib',n-1).require('fib',n-2).request()
    return call('fib',n-1) + call('fib',n-2)

@DLambda
def fib_even(n):
    # demonstrating cross-commit calls
    # fib_odd resides in another commit which is written in Java
    assert type(n) is int and n >= 0 and (n%2) == 1, str(type(n)) + ", " + str(n)
    return n if n <= 1 else call('fib_odd',n-1) + call('fib_even',n-2)

@DLambda
def n():
    # dummy node so that we can fiddle it
    return 0

@DLambda
def fib_fiddle():
    # dermonstrating Fiddles
    n = call('n') # this value is set by a Fiddle
    if n <= 1: return n
    # overriding Fiddled value of n and calling recursively
    with Fiddle('n',[],n-1):
        f1 = call('fib_fiddle')
    with Fiddle('n',[],n-2):
        f2 = call('fib_fiddle')
    return f1 + f2

@DLambda
def fib_fiddle_even():
    # demonstrating cross-commit fiddles
    n = call('n')
    assert type(n) is int and n >= 0 and (n%2) == 1, str(type(n)) + ", " + str(n)
    if n <= 1: return n
    with Fiddle('n',[],n-1):
        f1 = call('fib_fiddle_odd')
    with Fiddle('n',[],n-2):
        f2 = call('fib_fiddle_even')
    return f1 + f2

@DLambda
def test_fibonacci():
    # testing function call by computing Fibonacci(19)
    assert 4181 == call('fib',19)

    # testing cross-commit function calls
    # assert 4181 == call('fib_even',19)

    # testing Fiddles
    with Fiddle('n',[],19):
        assert 4181 == call('fib_fiddle')
    
    # testing cross-commit Fiddles
    # with Fiddle('n',[],19):
        # assert 4181 == call('fib_fiddle_even')
