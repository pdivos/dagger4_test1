from dagger4 import DFunction, DLambda, call, Fiddle

@DFunction
def test_functions():
    html = call('urlget','http://www.wikipedia.com')
    print(html)

@DFunction
def urlget(url):
    import urllib.request, urllib.error, urllib.parse
    assert type(url) is str
    res = urllib.request.urlopen(url)
    if res.info().get('Content-Encoding') == 'gzip':
        from io import StringIO
        import gzip
        buf = StringIO(res.read())
        f = gzip.GzipFile(fileobj=buf)
        return f.read()
    return res.read()
