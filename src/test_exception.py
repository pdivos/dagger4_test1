from dagger4 import DLambda, call, DException

@DLambda
def test_raise():
    raise Exception("Test Exception")

@DLambda
def test_rethrow():
    call('test_raise')

@DLambda
def test_except():
    try:
        call('test_rethrow')
    except DException as e:
        return e.args[0]

@DLambda
def test_exception():
    ret = call('test_except')
    assert type(ret) is str and "Test Exception" in ret
    print('test passed')