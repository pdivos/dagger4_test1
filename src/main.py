from dagger4 import DFunction, call, Fiddle, DepCollector

# import triggers executions of @DLambda and @DFunction decorators
import test_fibonacci
import test_functions
import test_objects
import test_exception

@DFunction
def main():
    DepCollector().require('test_fibonacci').require('test_functions').require('test_objects_py').require('test_exception').request()
    call('test_fibonacci')
    call('test_functions')
    call('test_objects_py')
    call('test_exception')
    print("All tests passed.")
