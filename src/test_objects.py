from dagger4 import DLambda, call, DepCollector

@DLambda
def test_dict_py():
    return {'a':1,2:'b',True:None}

@DLambda
def to_set_py(a,b,c):
    return {a,b,c}

@DLambda
def test_composite0_py(a, b):
    return {'c0_a':a,'c0_b':b}

@DLambda
def test_composite1_py():
    return {'c1_a':call('test_composite0_py',call('test_dict_py'),call('to_set_py',1,2,3)),'c1_b':call('to_set_py',1,2,3)}

@DLambda
def test_objects_py():
    assert call('test_composite1_py') == {'c1_a':{'c0_a':{'a':1,2:'b',True:None},'c0_b':{1,2,3}},'c1_b':{1,2,3}}
