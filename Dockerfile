FROM python:3.6
ENV PYTHONPATH="/usr/src/app"

# deps of dagger4_py are installed explicitly to avoid having to
# reinstall them each time dagger4_py changes which would be the case if they were not listed expicitly
RUN pip install --no-cache-dir websocket-client==0.47.0
RUN pip install --no-cache-dir msgpack==0.5.6
RUN pip install --no-cache-dir lru-dict==1.1.6
RUN pip install --no-cache-dir git+https://git@bitbucket.org/pdivos/dagger4_py.git@c95a32b

# deps required to run dcallables in the repo
RUN pip install --no-cache-dir requests==2.18.4

COPY ./src /usr/src/app
